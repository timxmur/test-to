package ru.timxmur.test.tochka.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.timxmur.test.tochka.domain.News;
import ru.timxmur.test.tochka.domain.NewsSource;

import java.util.List;
import java.util.Optional;

public interface NewsRepository extends JpaRepository<News, Long> {
    Optional<News> findOneByNewsId(Long news_id);
    Optional<News> findOneByTitle(String description);
    List<News> findOneBySource(NewsSource source);
    List<News> findAllByOrderByNewsIdDesc();
}
