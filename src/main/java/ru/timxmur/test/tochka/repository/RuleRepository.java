package ru.timxmur.test.tochka.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.timxmur.test.tochka.domain.Rule;

public interface RuleRepository extends JpaRepository<Rule, Long> {

}
