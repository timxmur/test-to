package ru.timxmur.test.tochka.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.timxmur.test.tochka.domain.News;
import ru.timxmur.test.tochka.domain.NewsSource;

import java.util.Optional;

public interface SourceRepository extends JpaRepository <NewsSource, Long> {
    Optional<NewsSource> findOneBySourceId(Long source_id);
}
