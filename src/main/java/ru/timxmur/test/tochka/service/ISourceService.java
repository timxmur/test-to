package ru.timxmur.test.tochka.service;

import ru.timxmur.test.tochka.domain.NewsSource;
import ru.timxmur.test.tochka.domain.NewsSourceForm;

import java.util.List;
import java.util.Optional;

public interface ISourceService {
    void delete(Long id);

    List<NewsSource> getAllSources();

    void update(NewsSourceForm form, Optional<Long> id);

    void save(NewsSource source);

    Optional<NewsSource> getById(Long id);
}
