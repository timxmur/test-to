package ru.timxmur.test.tochka.service;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.timxmur.test.tochka.domain.NewsSourceTypeEnum;


@Component
@RequiredArgsConstructor
public class ScheduledTasks {
    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);
    private final INewsService newsService;
    private final INewsRetrieverService retrieverService;
    private final ISourceService sourceService;


    @Scheduled(fixedRate = 20000)
    public void retrieveAndSaveNewsFromHtml() {
        sourceService.getAllSources().parallelStream().filter(e -> e.getType().equals(NewsSourceTypeEnum.HTML))
                .forEach(e -> retrieverService.retrieveHtml(e.getUri(), e.getRule())
                        .forEach(newsService::save));

    }

    @Scheduled(fixedRate = 20000)
    public void retrieveAndSaveNewsFromRss() {
        sourceService.getAllSources().parallelStream().filter(e -> e.getType().equals(NewsSourceTypeEnum.RSS))
                .forEach(e -> retrieverService.retrieveRss(e.getUri(), e.getRule())
                        .forEach(newsService::save));
    }
}