package ru.timxmur.test.tochka.service;

import ru.timxmur.test.tochka.domain.News;
import ru.timxmur.test.tochka.domain.NewsSource;

import java.util.List;
import java.util.Optional;

public interface INewsService {

    List<News> getAllNews();

    Optional<News> getById(Long id);

    void save(News news);

    void delete(Long id);

    List<News> getBySource(NewsSource source);
}
