package ru.timxmur.test.tochka.service;

import ru.timxmur.test.tochka.domain.News;
import ru.timxmur.test.tochka.domain.Rule;

import java.util.List;

public interface INewsRetrieverService {

    List<News> retrieveHtml(String url, Rule rule);

    List<News> retrieveRss(String url, Rule rule);

}
