package ru.timxmur.test.tochka.service;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import ru.timxmur.test.tochka.domain.News;
import ru.timxmur.test.tochka.domain.Rule;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class NewsRetrieverService implements INewsRetrieverService {
    private static final Logger logger = LoggerFactory.getLogger(NewsRetrieverService.class);

    @Override
    public List<News> retrieveRss(String url, Rule rule) {
        try {
            return Jsoup.connect(url).timeout(5000).get().getElementsByTag(rule.getTopElement())
                    .stream().map(element -> {
                        News news = new News();
                        news.setUrl(element.select(rule.getUrlRule()).text());
                        news.setDescription(element.select(rule.getContentRule()).text());
                        news.setTitle(element.select(rule.getTitleRule()).text());
                        news.setSource(rule.getSource());
                        return news;
                    }).collect(Collectors.toList());
        } catch (IOException e) {
            logger.error("Error occured while trying to parse {} with Rule={}, ", url, rule, e);
            return Collections.emptyList();
        }
    }


    @Override
    public List<News> retrieveHtml(String url, Rule rule) {
        try {
            return getFeed(Jsoup.connect(url).get().body(), rule.getTopElement(), rule.getTopElementType())
                    .orElseThrow(() -> new IOException("Cannot get a feed from: " + url + " with rule: " + rule))
                    .stream().map(element -> {
                        News news = new News();
                        news.setTitle(getFromElement(element, rule.getTitleRule(), rule.getTitleType()));
                        news.setDescription(getFromElement(element, rule.getContentRule(), rule.getContentType()));
                        news.setSource(rule.getSource());

                        String href = getFromElement(element, rule.getUrlRule(), rule.getUrlType());
                        news.setUrl(href.startsWith("http") ? href : rule.getUri() + href);
                        return news;
                    }).collect(Collectors.toList());
        } catch (IOException e) {
            logger.error("Error occured while trying to parse {} with Rule={}", url, rule);
            return Collections.emptyList();
        }
    }


    private Optional<Elements> getFeed(Element body, String name, String type) {
        if (Objects.isNull(body)) return Optional.empty();
        if (type.equals("class")) {
            return Optional.of(body.getElementsByClass(name));
        }
        if (type.equals("tag")) {
            return Optional.of(body.select(name));
        }
        return Optional.empty();
    }

    private String getFromElement(Element element, String name, String type) {
        if (element == null) return "";
        if (name == null || name.isEmpty()) return "";
        if (type == null || type.isEmpty()) return "";

        String[] typeSplitted = type.split(",");
        if (typeSplitted.length > 1) {
            String[] nameSplitted = name.split(",");
            Element e = null;
            if (typeSplitted[0].equals("class")) {
                e = element.getElementsByClass(nameSplitted[0]).first();
            }
            if (typeSplitted[0].equals("tag")) {
                e = element.selectFirst(nameSplitted[0]);
            }
            return getFromElement(e, name.substring(nameSplitted[0].length() + 1),
                    type.substring(typeSplitted[0].length() + 1));
        }

        if (type.equals("class"))
            return element.getElementsByClass(name).html();
        if (type.equals("tag"))
            return element.select(name).html();
        if (type.equals("attr"))
            return element.attr(name);
        return "";
    }

}
