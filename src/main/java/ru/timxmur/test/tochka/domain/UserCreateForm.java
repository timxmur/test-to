package ru.timxmur.test.tochka.domain;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@Data
public class UserCreateForm {
    @NotEmpty
    private String email = "";
    @NotEmpty
    private String password = "";
    @NotEmpty
    private String passwordRepeated = "";
    @NotNull
    private Role role = Role.USER;
    @NotEmpty
    private String firstname = "";
    @NotEmpty
    private String lastname = "";

    @Override
    public String toString() {
        return "UserCreateForm{" +
                "email='" + email.replaceFirst("@.+", "@***") + '\'' +
                ", password=***" + '\'' +           //because i'm not github or twitter
                ", passwordRepeated=***" + '\'' +
                ", role=" + role +
                ", firstname=" + firstname +
                ", lastname=" + lastname +
                '}';
    }
}
