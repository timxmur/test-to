package ru.timxmur.test.tochka.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@Setter
@Getter
public class NewsSourceForm {
    @NotEmpty
    private String name;
    @NotEmpty
    private String uri;
    @NotNull
    private NewsSourceTypeEnum type=NewsSourceTypeEnum.RSS;
    private String topElement="";
    private String topElementType="";
    private String titleRule="";
    private String titleType="";
    private String contentType="";
    private String urlType="";
    private String contentRule="";
    private String urlRule="";
}
