package ru.timxmur.test.tochka.domain;

import lombok.Data;

import javax.persistence.*;


@Entity
@Data
public class News {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="news_id", nullable = false, updatable = false)
    private Long newsId;

    @ManyToOne(cascade=CascadeType.MERGE)
    private NewsSource source;
    @Column(name="title")
    private String title;
    @Column(name="url")
    private String url;
    @Column(name="description", length = 50000)
    private String description;
}
