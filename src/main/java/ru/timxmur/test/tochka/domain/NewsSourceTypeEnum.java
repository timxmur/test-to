package ru.timxmur.test.tochka.domain;

public enum NewsSourceTypeEnum {
    RSS, HTML
}
