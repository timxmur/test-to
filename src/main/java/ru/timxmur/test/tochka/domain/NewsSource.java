package ru.timxmur.test.tochka.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class NewsSource {
    @Id
    @GeneratedValue(generator = "sequence", strategy=GenerationType.SEQUENCE)
    @SequenceGenerator(name = "sequence", allocationSize = 10)
    @Column(name="source_id", nullable = false, updatable = false, unique = true)
    private Long sourceId;
    @Column(name="name")
    private String name;
    @Column(name="uri")
    private String uri;
    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    private NewsSourceTypeEnum type;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "source", cascade = CascadeType.ALL)
    private Rule rule;
}
