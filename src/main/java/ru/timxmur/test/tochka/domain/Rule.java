package ru.timxmur.test.tochka.domain;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Rule {
    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    private NewsSource source;

    @GenericGenerator(name = "generator", strategy = "foreign",
            parameters = @Parameter(name = "property", value = "source"))
    @Id
    @GeneratedValue(generator = "generator")
    @Column(name = "rule_id", unique = true, nullable = false)
    private Long ruleId;

    private String uri;
    private String topElement;
    private String topElementType;
    private String titleRule;
    private String titleType;
    private String contentRule;
    private String contentType;
    private String urlRule;
    private String urlType;


    @Override
    public String toString() {
        return
                "uri='" + uri + '\'' +
                        ", topElement='" + topElement + '\'' +
                        ", topElementType='" + topElementType + '\'' +
                        ", titleRule='" + titleRule + '\'' +
                        ", titleType='" + titleType + '\'' +
                        ", contentRule='" + contentRule + '\'' +
                        ", contentType='" + contentType + '\'' +
                        ", urlType='" + urlType + '\'' +
                        ", urlRule='" + urlRule + '\'';
    }
}
