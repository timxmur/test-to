package ru.timxmur.test.tochka.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.timxmur.test.tochka.service.UserService;

@Controller
@RequiredArgsConstructor
public class UsersController {

    private final UserService userService;

    @RequestMapping("/users")
    public ModelAndView getUsersPage() {
        return new ModelAndView("users", "users", userService.getAllUsers());
    }

}
