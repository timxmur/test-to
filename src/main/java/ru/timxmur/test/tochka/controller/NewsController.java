package ru.timxmur.test.tochka.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.timxmur.test.tochka.service.INewsService;

@Controller
@RequiredArgsConstructor
public class NewsController {
    private final INewsService newsService;

    @RequestMapping("/news")
    public ModelAndView getNewsPage() {
        return new ModelAndView("news", "feed", newsService.getAllNews());
    }
}
