INSERT INTO user_table (email, password_hash, role, firstname, lastname)
VALUES ('demo@localhost', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'ADMIN', 'demo', 'demo');
INSERT INTO news_source (source_id, name, type, uri) VALUES (1, 'ixbt', 'RSS', 'http://www.ixbt.com/export/news.rss');
INSERT INTO news_source (source_id, name, type, uri) VALUES (2, 'breakingmad', 'HTML', 'http://breakingmad.me/ru/');
INSERT INTO public.rule (rule_id, content_rule, content_type, title_rule, title_type, top_element, top_element_type, uri, url_rule, url_type) VALUES (1, 'description', '',  'title', '', 'item', '', null, 'link', '');
INSERT INTO public.rule (rule_id, content_rule, content_type, title_rule, title_type, top_element, top_element_type, uri, url_rule, url_type) VALUES (2, 'p', 'tag', 'h2', 'tag', 'news-row', 'class', null, 'ico-link news-bottom-link,href', 'class,attr');