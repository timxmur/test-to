<#-- @ftlvariable name="_csrf" type="org.springframework.security.web.csrf.CsrfToken" -->
<#-- @ftlvariable name="sources" type="java.util.List<NewsSource>" -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Список источников</title>
    <link rel="stylesheet" type="text/css" href="/css/style.css">
</head>
<body>
<div class="row">
    <nav role="navigation">
        <ul>
    <#if !currentUser??>
        <li><a href="/login">Войти</a></li>
    </#if>
    <#if currentUser??>
        <li>
            <form action="/logout" method="post">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <button type="submit">Выйти</button>
            </form>
        </li>
        <li><a href="/news">Новости</a></li>
        <#if currentUser.role == "ADMIN" >
            <li><a href="/sources">Источники новостей</a></li>
            <li><a href="/users">Пользователи</a></li>
        </#if>
    </#if>
        </ul>
    </nav>

    <h1>Источники новостей</h1>
    <div>
        <a href="/sources/create" class="button-c">Добавить источник новостей</a>
    </div>
    <table class="table-sources">
        <thead>
        <tr>
            <th>Название</th>
            <th>Тип</th>
            <th>Правила</th>
            <th>URI</th>
        </tr>
        </thead>
        <tbody>
    <#list sources as source>
    <tr>
        <td>${source.name}</td>
        <td>${source.type}</td>
        <td>${source.rule}</td>
        <td>${source.uri}</td>
        <#if currentUser??>
        <td><a class="button-e" href="/sources/edit/${source.sourceId}">Edit</a></td>
        <td>
            <form role="form" name="form" action="/sources/delete/${source.sourceId}">
                <button type="submit">Delete</button>
            </form>
        </td>
        </#if>

    </tr>
    </#list>
        </tbody>
    </table>
</div>
</body>
</html>