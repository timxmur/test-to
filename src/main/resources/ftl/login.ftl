<#-- @ftlvariable name="_csrf" type="org.springframework.security.web.csrf.CsrfToken" -->
<#-- @ftlvariable name="error" type="java.util.Optional<String>" -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Войти</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div class="row">
<p>Для входа можно использовать: demo@localhost / demo</p>

<form role="form" action="/login" method="post">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

    <div class="inp">
        <label for="email">Email</label>
        <input type="email" name="email" id="email" required autofocus/>
    </div>
    <div class="inp">
        <label for="password">Пароль</label>
        <input type="password" name="password" id="password" required/>
    </div>
    <div class="check">
        <label for="remember-me">Запомнить меня</label>
        <input type="checkbox" name="remember-me" id="remember-me"/>
    </div>
    <button type="submit">Sign in</button>
</form>

<#if error.isPresent()>
<p>The email or password you have entered is invalid, try again.</p>
</#if>
</div>
</body>
</html>