<#-- @ftlvariable name="_csrf" type="org.springframework.security.web.csrf.CsrfToken" -->
<#-- @ftlvariable name="form" type="UserCreateForm" -->
<#import "/spring.ftl" as spring>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Создание пользователя</title>
    <link rel="stylesheet" type="text/css" href="/css/style.css">
</head>
<body>
<div class="row">
    <nav role="navigation">
        <ul>
    <#if !currentUser??>
        <li><a href="/login">Войти</a></li>
    </#if>
    <#if currentUser??>
        <li>
            <form action="/logout" method="post">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <button type="submit">Выйти</button>
            </form>
        </li>
        <li><a href="/news">Новости</a></li>
        <#if currentUser.role == "ADMIN" >
            <li><a href="/sources">Источники новостей</a></li>
            <li><a href="/users">Пользователи</a></li>
        </#if>
    </#if>
        </ul>
    </nav>
<h1>Добавить пользователя</h1>

<form role="form" name="form" action="" method="post">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

    <div class="inp">
        <label for="email">Email</label>
        <input type="email" name="email" id="email" value="${form.email}" required autofocus/>
    </div>
    <div class="inp">
        <label for="password">Пароль</label>
        <input type="password" name="password" id="password" required/>
    </div>
    <div class="inp">
        <label for="passwordRepeated">Повторите пароль</label>
        <input type="password" name="passwordRepeated" id="passwordRepeated" required/>
    </div>
    <div class="inp">
        <label for="firstname">Имя</label>
        <input type="text" name="firstname" id="firstname" required/>
    </div>

    <div class="inp">
        <label for="lastname">Фамилия</label>
        <input type="text" name="lastname" id="lastname" required/>
    </div>

    <div class="inp">
        <label for="role">Роль</label>
        <select name="role" id="role" required>
            <#if currentUser.role == "ADMIN" ><option <#if form.role == 'USER'>selected</#if>>USER</option></#if>
           <#if currentUser.role == "ADMIN"><option <#if form.role == 'ADMIN'>selected</#if>>ADMIN</option></#if>
        </select>
    </div>
    <button type="submit">Save</button>
</form>

<@spring.bind "form" />
<#if spring.status.error>
<ul>
    <#list spring.status.errorMessages as error>
        <li>${error}</li>
    </#list>
</ul>
</#if>
</div>
</body>
</html>