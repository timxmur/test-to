<#-- @ftlvariable name="users" type="java.util.List<User>" -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Список пользователей</title>
    <link rel="stylesheet" type="text/css" href="/css/style.css">
</head>
<body>
<div class="row">
    <nav role="navigation">
        <ul>
    <#if !currentUser??>
        <li><a href="/login">Войти</a></li>
    </#if>
    <#if currentUser??>
        <li>
            <form action="/logout" method="post">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <button type="submit">Выйти</button>
            </form>
        </li>
        <li><a href="/news">Новости</a></li>
        <#if currentUser.role == "ADMIN" >
            <li><a href="/sources">Источники новостей</a></li>
            <li><a href="/users">Пользователи</a></li>
        </#if>
    </#if>
        </ul>
    </nav>
<h1>Пользователи</h1>
    <div>
        <a href="/user/create" class="button-c">Добавить пользователя</a>
    </div>
<table>
    <thead>
    <tr>
        <th>E-mail</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Role</th>
    </tr>
    </thead>
    <tbody>
    <#list users as user>
    <tr>
        <td><a href="/user/${user.id}">${user.email}</a></td>
        <td>${user.firstname}</td>
        <td>${user.lastname}</td>
        <td>${user.role}</td>
    </tr>
    </#list>
    </tbody>
</table>
</div>
</body>
</html>