<#-- @ftlvariable name="_csrf" type="org.springframework.security.web.csrf.CsrfToken" -->
<#-- @ftlvariable name="form" type="NewsSourceForm" -->
<#-- @ftlvariable name="source" type="NewsSource" -->

<#import "/spring.ftl" as spring>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Изменить источник новостей</title>
    <link rel="stylesheet" type="text/css" href="/css/style.css">
</head>
<body>
<div class="row">
    <nav role="navigation">
        <ul>
    <#if !currentUser??>
        <li><a href="/login">Войти</a></li>
    </#if>
    <#if currentUser??>
        <li>
            <form action="/logout" method="post">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <button type="submit">Выйти</button>
            </form>
        </li>
        <li><a href="/news">Новости</a></li>
        <#if currentUser.role == "ADMIN" >
            <li><a href="/sources">Источники новостей</a></li>
            <li><a href="/users">Пользователи</a></li>
        </#if>
    </#if>
        </ul>
    </nav>

    <h1>Изменить источник новостей</h1>

    <form role="form" name="form" action="/sources/edit/${source.sourceId}" method="post" class="form_create">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <div class="form_create-block">
            <div class="inp">
                <label for="type">Тип</label>
                <select name="type" id="type" required>
                    <option <#if source.type == 'RSS'>selected</#if>>RSS</option>
                    <option <#if source.type == 'HTML'>selected</#if>>HTML</option>
                </select>
            </div>
            <div class="inp">
                <label for="name">name</label>
                <input type="text" name="name" id="name" value="${source.name}" required autofocus/>
            </div>
            <div class="inp">
                <label for="uri">uri</label>
                <input type="text" name="uri" id="uri" value="${source.uri}" required/>
            </div>
            <button type="submit">Save</button>
        </div>

        <div class="form_create-block2">
            <div class="inp">
                <label for="topElement">topElement</label>
                <input type="text" name="topElement" id="topElement" value="${source.rule.topElement}"/>
            </div>
            <div class="inp">
                <label for="topElementType">topElementType</label>
                <input type="text" name="topElementType" id="topElementType" value="${source.rule.topElementType}"/>
            </div>
        </div>
        <div class="form_create-block2">
            <div class="inp">
                <label for="titleRule">titleRule</label>
                <input type="text" name="titleRule" id="titleRule" value="${source.rule.titleRule}"/>
            </div>
            <div class="inp">
                <label for="titleType">titleType</label>
                <input type="text" name="titleType" id="titleType" value="${source.rule.titleType}"/>
            </div>
        </div>
        <div class="form_create-block2">
            <div class="inp">
                <label for="contentRule">contentRule</label>
                <input type="text" name="contentRule" id="contentRule" value="${source.rule.contentRule}"/>
            </div>
            <div class="inp">
                <label for="contentType">contentType</label>
                <input type="text" name="contentType" id="contentType" value="${source.rule.contentType}"/>
            </div>
        </div>
        <div class="form_create-block2">
            <div class="inp">
                <label for="urlRule">urlRule</label>
                <input type="text" name="urlRule" id="urlRule" value="${source.rule.urlRule}"/>
            </div>
            <div class="inp">
                <label for="urlType">urlType</label>
                <input type="text" name="urlType" id="urlType" value="${source.rule.urlType}"/>
            </div>
        </div>
    </form>
</div>
</body>
</html>